<?php

namespace Drupal\vd;

/**
 * Twig extension for vd.
 */
class VdTwigExtension extends \Twig_Extension {

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('vd', 'vd'),
    ];
  }

}
